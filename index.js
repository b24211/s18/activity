// Addition Function
function addFunction(num1, num2){
	let sum = num1 + num2;
	console.log("Displayed sum of " + num1 + " and " + num2 + ":");
	console.log(sum);
}
function invokeAdd(addFunction){
	addFunction(5, 15);
}
invokeAdd(addFunction);

// Subtraction Function
function subtractFunction(num1, num2){
	let difference = num1 - num2;
	console.log("Displayed difference of " + num1 + " and " + num2 + ":");
	console.log(difference);
}
function invokeSubtract(subtractFunction){
	subtractFunction(20, 5);
}
invokeSubtract(subtractFunction);

// Multiplication Function
function MultiplyFunction(num1, num2){
	let product = num1 * num2;
	console.log("Displayed product of " + num1 + " and " + num2 + ":");
	return product;
}
let product = MultiplyFunction(50, 10);
console.log(product);

// Division Function
function divideFunction(num1, num2){
	let quoitient = num1 / num2;
	console.log("Displayed quoitient of " + num1 + " and " + num2 + ":");
	return quoitient;
}
let quoitient = divideFunction(50, 10);
console.log(quoitient);

// Area of a circle function
function areaFunction(radius){
	let area = (22/7)*radius*radius;
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	return area;
}
let circleArea = areaFunction(15);
console.log(circleArea);

// Average Function
function averageFunction(num1, num2, num3, num4){
	let average = (num1 + num2 + num3 + num4)/4;
	console.log("The average of " + num1 + "," + num2 + "," + num3 + " and " + num4 + ":");
	return average;
}
let averageVar = averageFunction(20,40,60,80);
console.log(averageVar);

// Percentage Function
function percentageFunction(score, total){
	let percentage = score/total;
	let isPassed = percentage > 0.75;
	console.log("Is " + percentage + " a passing score?");
	return isPassed;
}
let isPassingScore = percentageFunction(38,50);
console.log(isPassingScore);